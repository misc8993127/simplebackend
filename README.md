# A simple API and back end demo

To install dependencies:

```bash
bun install
```

To run:

```bash
bun run dev
```

Get a list of users

```
curl http://localhost:3000/users
```

Get a specified user by id

```
curl http://localhost:3000/users/2
```

Create a user

```
curl -X POST -H "Content-Type: application/json" -d '{"id":1, "first_name":"John", "last_name":"Doe", "email":"john@example.com"}' http://localhost:3000/users
```

Delete a user

```
curl -X DELETE http://localhost:3000/users/1
```

Update a User

```
curl -X PUT -H "Content-Type: application/json" -d '{"first_name":"John", "last_name":"Doe", "email":"john.doe@example.com"}' http://localhost:3000/users/7
```
