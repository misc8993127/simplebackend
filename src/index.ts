import _, { get } from "lodash";
import cors from "cors";

const express = require("express");
const bodyParser = require("body-parser");

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));

app.use(bodyParser.json());
const corsOptions = {
  origin: [
    "http://localhost:5173",
    "https://simplefrontend-snowy.vercel.app/",
    "*", // allow all
  ],
};
app.use(cors(corsOptions));

const port = 3000;

interface User {
  id: number | string;
  first_name: string;
  last_name: string;
  avatar: string;
}

interface UserResponse {
  page: number;
  per_page: number;
  total: number;
  total_pages: number;
  data: User[];
}

let users: User[] = [];

const getUsers = async () => {
  const response: UserResponse = await fetch(
    "https://reqres.in/api/users?page=2",
    {
      headers: {
        "content-type": "application/json",
      },
      body: null,
      method: "GET",
    }
  ).then((response) => response.json());

  users = response.data;
};

getUsers();

app.get("/", (req: any, res: { send: (arg0: string) => void }) => {
  res.send("Hello This is a simple API");
});

app.get("/users", (req: any, res: { json: (arg0: User[]) => void }) => {
  res.json(users);
});

app.get(
  "/users/:id",
  (
    req: { params: { id: any } },
    res: {
      json: (arg0: User) => void;
      status: (arg0: number) => {
        (): any;
        new (): any;
        json: { (arg0: { message: string }): void; new (): any };
      };
    }
  ) => {
    const id = req.params.id;
    const user = users.find((user) => user.id === Number(id));
    if (user) {
      res.json(user);
    } else {
      res.status(404).json({ message: "User not found" });
    }
  }
);

app.post(
  "/users",
  (
    req: { body: any },
    res: {
      status: (arg0: number) => {
        (): any;
        new (): any;
        json: { (arg0: { message: string }): void; new (): any };
      };
      json: (arg0: { success: boolean; message: string; data: any }) => void;
    }
  ) => {
    console.log("a new users,", req.body);
    const newUser = req.body;

    const user = users.find((user) => user.id === Number(newUser.id));
    if (user) {
      res.status(400).json({ message: "User already exists :(" });
      return;
    }
    users.push(newUser);
    res.json({
      success: true,
      message: "New User Created",
      data: newUser,
    });
  }
);

app.delete(
  "/users/:id",
  (
    req: { params: { id: any } },
    res: {
      status: (arg0: number) => {
        (): any;
        new (): any;
        json: {
          (arg0: { message: string; success: boolean }): void;
          new (): any;
        };
      };
      json: (arg0: { message: string; success: boolean }) => void;
    }
  ) => {
    const id = req.params.id;
    const index = users.findIndex((user) => user.id === id);

    if (index === -1) {
      res
        .status(404)
        .json({
          message: `User not found id:  This should work ${id}`,
          success: false,
        });
      return;
    }

    users.splice(index, 1);
    res.json({ message: "User deleted successfully", success: true });
  }
);

app.put(
  "/users/:id",
  (
    req: { params: { id: any }; body: any },
    res: {
      status: (arg0: number) => {
        (): any;
        new (): any;
        json: {
          (arg0: { message: string; success: boolean }): void;
          new (): any;
        };
      };
      json: (arg0: { success: boolean; message: string; data: any }) => void;
    }
  ) => {
    const id = req.params.id;
    const index = users.findIndex((user) => user.id === id);

    if (index === -1) {
      res
        .status(404)
        .json({ message: `User not found id ${id}`, success: false });
      return;
    }

    const user = users[index];
    const updates = req.body;
    const updatedUser = { ...user, ...updates };

    users[index] = updatedUser;

    res.json({
      success: true,
      message: "User Updated",
      data: updatedUser,
    });
    return;
  }
);

app.get(
  "/reset",
  (
    req: any,
    res: {
      json: (arg0: { message: string; success: boolean; data: User[] }) => void;
    }
  ) => {
    getUsers().then(() => {
      res.json({
        message: "Users reset successfully",
        success: true,
        data: users,
      });
    });
  }
);

app.listen(port, "0.0.0.0", () => {
  console.log(`Server started on http://localhost:${port}`);
});
