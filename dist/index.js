"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cors_1 = __importDefault(require("cors"));
const express = require("express");
const bodyParser = require("body-parser");
const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
const corsOptions = {
    origin: [
        "http://localhost:5173",
        "https://simplefrontend-snowy.vercel.app/",
        "*", // allow all
    ],
};
app.use((0, cors_1.default)(corsOptions));
const port = 3000;
let users = [];
const getUsers = () => __awaiter(void 0, void 0, void 0, function* () {
    const response = yield fetch("https://reqres.in/api/users?page=2", {
        headers: {
            "content-type": "application/json",
        },
        body: null,
        method: "GET",
    }).then((response) => response.json());
    users = response.data;
});
getUsers();
app.get("/", (req, res) => {
    res.send("Hello This is a simple API");
});
app.get("/users", (req, res) => {
    res.json(users);
});
app.get("/users/:id", (req, res) => {
    const id = req.params.id;
    const user = users.find((user) => user.id === Number(id));
    if (user) {
        res.json(user);
    }
    else {
        res.status(404).json({ message: "User not found" });
    }
});
app.post("/users", (req, res) => {
    console.log("a new users,", req.body);
    const newUser = req.body;
    const user = users.find((user) => user.id === Number(newUser.id));
    if (user) {
        res.status(400).json({ message: "User already exists :(" });
        return;
    }
    users.push(newUser);
    res.json({
        success: true,
        message: "New User Created",
        data: newUser,
    });
});
app.delete("/users/:id", (req, res) => {
    const id = Number(req.params.id);
    const index = users.findIndex((user) => user.id === id);
    if (index === -1) {
        res
            .status(404)
            .json({ message: `User not found id ${id}`, success: false });
        return;
    }
    users.splice(index, 1);
    res.json({ message: "User deleted successfully", success: true });
});
app.put("/users/:id", (req, res) => {
    const id = Number(req.params.id);
    const index = users.findIndex((user) => user.id === id);
    if (index === -1) {
        res
            .status(404)
            .json({ message: `User not found id ${id}`, success: false });
        return;
    }
    const user = users[index];
    const updates = req.body;
    const updatedUser = Object.assign(Object.assign({}, user), updates);
    users[index] = updatedUser;
    res.json({
        success: true,
        message: "User Updated",
        data: updatedUser,
    });
    return;
});
app.get("/reset", (req, res) => {
    getUsers().then(() => {
        res.json({
            message: "Users reset successfully",
            success: true,
            data: users,
        });
    });
});
app.listen(port, "0.0.0.0", () => {
    console.log(`Server started on http://localhost:${port}`);
});
//# sourceMappingURL=index.js.map